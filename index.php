<?php
require('mvc/controller/fonctions.php');
require('mvc/controller/treatment.php');
require('mvc/controller/router.php');

require('mvc/view/head.php');
require('mvc/view/navbar.php');
require("mvc/view/$page.php");
require('mvc/view/navBottom.php');
require('mvc/view/foot.php');