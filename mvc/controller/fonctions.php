<?php
function checkRights($psudo='', $password=''){
    
    if($psudo==''){
        if($_REQUEST['idForm'] == 'login'){
            return false;
        }
        if(!empty($_COOKIE['psudo'])){
            $psudo = $_COOKIE['psudo'];
        } else {
            return false;
        }
    }
    
    if($password==''){
        if($_REQUEST['idForm'] == 'login'){
            return false;
        }
        if(!empty($_COOKIE['password'])){
            $password = $_COOKIE['password'];
        } else {
            return false;
        }
    }
    $ObjFichierCsv = new SplFileObject('mvc/model/login.csv');
    $ObjFichierCsv->setFlags(SplFileObject::READ_CSV | SplFileObject::READ_AHEAD | SplFileObject::SKIP_EMPTY | SplFileObject::DROP_NEW_LINE);
    $ObjFichierCsv->setCsvControl(',');
    while ($account = $ObjFichierCsv->fgetcsv()){
            if(($account[0] == $psudo) && ($account[1] == $password)){
                return true;
            }
    }
    
    return false;
}