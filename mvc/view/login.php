<?php
if(checkRights()){
   header('Location: ./?page=workspace');
}
?>
<form class="form-horizontal container" role="form" method="POST">
<fieldset>
    <legend>Login:</legend>
      <div class="form-group">
        <label class="control-label col-sm-2" for="psudo">Psudo:</label>
        <div class="col-sm-10"> 
          <input type="text" class="form-control" name="psudo" placeholder="login">
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-2" for="password">Mdp:</label>
        <div class="col-sm-10"> 
          <input type="password" class="form-control" name="password" placeholder="password">
        </div>
      </div>
      <div class="form-group"> 
        <div class="col-sm-offset-2 col-sm-10">
          <input type="hidden" name="idForm" value="login">
          <button type="submit" class="btn btn-default">Submit</button>
        </div>
      </div>
<fieldset>
</form>
<?php
if(!empty($_REQUEST['idForm'])){
    echo '<div class="alert alert-danger" role="alert">Psudo and/or password incorrect!</div>';
}
?>